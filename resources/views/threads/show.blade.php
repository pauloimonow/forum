@extends('layouts.app')

@section('header')

    <link href="{{ asset('css/vendor/jquery.atwho.css') }}" rel="stylesheet">

@endsection

@section('content')

    <thread-view :thread="{{ $thread }}" inline-template>
        <div class="container">
            <div class="row" v-cloak>
                <div class="col-md-8">
                    @include('threads._question')

                    <replies @added="repliesCount++" @removed="repliesCount--"></replies>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>
                                This thread was published {{ $thread->created_at->diffForHumans() }} by <a href="#">{{ $thread->creator->name }}</a>, and currently has <span v-text="repliesCount"></span> comments.
                            </p>

                            <subscribe-button :active="{{ json_encode($thread->isSubscribedTo) }}" v-if="signedIn"></subscribe-button>

                            <button type="button" class="btn btn-default" v-if="authorize('isAdmin')" @click="toggleLock" v-text="locked ? 'Unlock' : 'Lock'"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </thread-view>

@endsection
